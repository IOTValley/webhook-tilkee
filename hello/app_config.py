from django.apps import AppConfig

from .startup import startup

class MyAppConfig(AppConfig):
    name = 'hello'

    def ready(self):

        print("Starting the App!")
        startup()