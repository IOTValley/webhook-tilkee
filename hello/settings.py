
import json

def get_pipedrive_token():
	
	with open('settings.json', 'r') as f:
		config = json.load(f)

		return config["PIPEDRIVE"]["API_TOKEN"]