import json

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from hello.pipedrive import get_deals_from_filtre

# Create your views here.
def index(request):

	return HttpResponse("<h1>Bonjour</h1><p>Serveur Up!</p>")


@csrf_exempt
def receive_tilkee_webhook(request):
	if request.method == 'POST':

		print(request.body)
		data = json.loads(request.body)

		get_deals_from_filtre(data['token'])

		return HttpResponse("<h1>Bonjour</h1><p>Bonne methode!</p>")

	else:
		return HttpResponse("<h1>Bonjour</h1><p>Mauvaise requete envoye!</p>")