import os
import json
import requests

from .settings import get_pipedrive_token


PIPEDRIVE_API_TOKEN = os.environ.get('PIPEDRIVE_TOKEN', None)

def get_deals_from_filtre(webhook_data):
    PIPEDRIVE_TILKEE_FIELDS_ID = None
    
    url = 'https://api.pipedrive.com/v1/deals?limit=500&api_token={}'.format(PIPEDRIVE_API_TOKEN)
    headers = {'Accept': 'application/json'}

    r = requests.get(url=url, headers=headers)

    if r.status_code == 200:
               
        PIPEDRIVE_TILKEE_FIELDS_ID = get_tilkee_field_id()
        if PIPEDRIVE_TILKEE_FIELDS_ID == None:
            print('Impossible to reach the tilkee fields in your Pipedrive configuration. Aborted.')
            return None

        data = json.loads(r.text)
        for row in data['data']:
            
            if row[PIPEDRIVE_TILKEE_FIELDS_ID] != None:
                if row[PIPEDRIVE_TILKEE_FIELDS_ID].find(webhook_data['url']) != -1:
                    # On verifie d'abord si une activité lié à ce tilkee link n'est pas déjà créé. 
                    activity_id = check_tilkeeview_already_exist(row['id'], webhook_data['url'])
                    if activity_id != None:
                        #Si c'est le cas, on l'update
                        update_tilkeeview_activity(activity_id, webhook_data)

                    else:
                        # Si ce n'est pas le cas, on la créer 
                        add_tilkee_activity(row['id'], webhook_data)

        while data['additional_data']['pagination']["more_items_in_collection"] == True:
            
            next_start = str(data['additional_data']['pagination']["next_start"])
            url = 'https://api.pipedrive.com/v1/deals?limit=500&start={}&api_token={}'.format(next_start, PIPEDRIVE_API_TOKEN)
            r = requests.get(url=url, headers=headers)
            data = json.loads(r.text)

            for row in data['data']:
                
                if row[PIPEDRIVE_TILKEE_FIELDS_ID] != None:
                    if row[PIPEDRIVE_TILKEE_FIELDS_ID].find(webhook_data['url']) != -1:
                        # On verifie d'abord si une activité lié à ce tilkee link n'est pas déjà créé. 
                        activity_id = check_tilkeeview_already_exist(row['id'], webhook_data['url'])
                        if activity_id != None:
                            #Si c'est le cas, on l'update
                            update_tilkeeview_activity(activity_id, webhook_data)

                        else:
                            # Si ce n'est pas le cas, on la créer 
                            add_tilkee_activity(row['id'], webhook_data)

    else:
        print('request fail with status_code : '+str(r.status_code))
        print(r.text)

        return None

def check_tilkeeview_already_exist(deal_id, tilkee_link_url):

    url = 'https://api.pipedrive.com/v1/deals/{}/activities?start=0&api_token={}'.format(str(deal_id), PIPEDRIVE_API_TOKEN)
    headers = {"Accept": "application/json"}

    r = requests.get(url=url, headers=headers)

    if r.status_code == 200:
        data = json.loads(r.text)
        
        if data['data'] == None:
            return None

        for row in data['data']:  
        
            if row['type'] == 'tilkeeview':
        
                note = row['note']
                if note.find(tilkee_link_url) != -1:
                    return row['id']

        return None

    else:

        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return None


def add_tilkee_activity(deal_id, webhook_data):

    m, s = divmod(webhook_data['total_time'], 60)
    h, m = divmod(m, 60)
    total_time = "%dh:%02dm:%02ds" % (h, m, s)

    # TODO finir cette fonction.
    note =  """<p>Url du lien Tilkee : {}</p>
            <p>Pourcentage de la présentation lu : {} %</p>
            <p>Temps total passé sur la présentation : {}</p>
            <p>Nombre de connexion à la présentation : {}""".format(webhook_data['url'], 
                webhook_data['percentage_read'], total_time, webhook_data['connexion_count'])

    url = 'https://api.pipedrive.com/v1/activities?type=tilkeeview&=500&api_token={}'.format(PIPEDRIVE_API_TOKEN)

    headers = {'Accept': 'application/json'}
    body = {
        "subject": "Tilkee View du lien {}".format(webhook_data['url']),
        "done": "1",
        "type": "tilkeeview",
        "deal_id": deal_id,
        "note": note
    }

    r = requests.post(url=url, headers=headers, data=body)

    if r.status_code == 201:
        print('Ajout de l\'activite ok')

    else:

        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return None


def update_tilkeeview_activity(id_activity, webhook_data):

    m, s = divmod(webhook_data['total_time'], 60)
    h, m = divmod(m, 60)
    total_time = "%dh:%02dm:%02ds" % (h, m, s)

    # TODO finir cette fonction.
    note =  """<p>Url du lien Tilkee : {}</p>
            <p>Pourcentage de la présentation lu : {} %</p> 
            <p>Temps total passé sur la présentation : {}</p>
            <p>Nombre de connexion à la présentation : {}""".format(webhook_data['url'], 
                webhook_data['percentage_read'], total_time, webhook_data['connexion_count'])

    url = 'https://api.pipedrive.com/v1/activities/{}?api_token={}'.format(str(id_activity), PIPEDRIVE_API_TOKEN)
    headers = {'Accept': 'application/json'}
    
    body = {
        "id": "{}".format(str(id_activity)),
        "done": "1",
        "note": note
    }

    r = requests.put(url=url, headers=headers, data=body)

    if r.status_code == 200:
        print('Update de l\'activite ok')

    else:

        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return None


def get_tilkee_field_id():

    url = "https://api.pipedrive.com/v1/dealFields?api_token="+PIPEDRIVE_API_TOKEN
    headers = {'Accept': 'application/json'}
    
    r = requests.get(url=url, headers=headers)

    if r.status_code == 200:
        data = json.loads(r.text)
        return process_response_tilkee_field(data['data'])
    else:
        return None


def process_response_tilkee_field(dealfields):

    cpt = 0
    name = dealfields[cpt]['name'].lower()
    id = ""
    if name == "tilkee" or name == "tilkee_link" or name == "lien_tilkee" :
        id = dealfields[cpt]['key']

    cpt += 1

    while id == "" and cpt < len(dealfields): 
        name_tmp = dealfields[cpt]['name'].lower()
        if name_tmp == "tilkee" or name_tmp == "tilkee_link" or name_tmp == "lien_tilkee":
            id = dealfields[cpt]['key']
        
        cpt += 1

    if id != "":
        return id
    else:
        return None