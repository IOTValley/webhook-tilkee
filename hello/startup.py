# startup.py
import os
import json
import requests


def startup():

    tilkee_info = {
        'password' : os.environ.get('TILKEE_PASSWORD', None),
        'username' : os.environ.get('TILKEE_USERNAME', None),
        'useremail' : os.environ.get('TILKEE_USEREMAIL', None),
        'x-tilk-ref' : os.environ.get('X_TILK_REF', None),
        'webhook_url' : os.environ.get('WEBHOOK_URL', None),
        'token' : None
    }

    print(tilkee_info)

    tilkee_info['token'] = get_tilkee_auth_token(tilkee_info)

    if tilkee_info['token'] is not None:

        if is_webhook_exist(tilkee_info) :
            print("Webhook already exist, let's start the app.")
        else:
            print("Webhook not exist, let's create it.")
            company_id = get_tilkee_company_info(tilkee_info)
            create_webhook(company_id, tilkee_info)
            print("Let's start the app!")


def is_webhook_exist(tilkee_info):

    url = 'https://api.tilkee.com/webhooks'

    headers = {
        "Authorization" :"Bearer {}".format(tilkee_info['token']),
        "x-tilk-ref": tilkee_info['x-tilk-ref'],
        "USER_MAIL": tilkee_info['useremail'],
        "Accept": "application/json"
    }

    r = requests.get(url=url, headers=headers)

    if r.status_code == 200:
        data = json.loads(r.text)
        print("webhook informations succefully get : {}".format(data))

        if len(data['contents']) != 0:
            return True
        else:
            return False

    else :
        print("Error while accessing the tilkee webhook informations.")
        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return True    


def create_webhook(company_id, tilkee_info):

    url = 'https://api.tilkee.com/webhooks'

    headers = {
        "Authorization" :"Bearer {}".format(tilkee_info['token']),
        "x-tilk-ref": tilkee_info['x-tilk-ref'],
        "USER_MAIL": tilkee_info['useremail'],
        "Accept": "application/json"
    }

    body = {
        "url": tilkee_info['webhook_url'],
        "notifed_by_start": False,
        "web_type": "ConnexionEnd",
        "entity_id": int(company_id),
        "entity_type": "Company"
    }

    r = requests.post(url=url, headers=headers, data=body)

    if r.status_code == 200:
        data = json.loads(r.text)
        print("Succefully created the new Webhook with id {}.".format(data['id'])) 
    else:
        print("Error while creating the new webhook.")
        print('request fail with status_code : '+str(r.status_code))
        print(r.text)


def get_tilkee_company_info(tilkee_info):

    url = 'https://api.tilkee.com/companies/mine'

    headers = {
        "Authorization" :"Bearer {}".format(tilkee_info['token']),
        "x-tilk-ref": tilkee_info['x-tilk-ref'],
        "USER_MAIL": tilkee_info['useremail'],
        "Accept": "application/json",
    }

    r = requests.get(url=url, headers=headers)
    
    if r.status_code == 200:
        print("Success while accessing id of companies : {}".format(json.loads(r.text)))
        data = json.loads(r.text)
        return data['id']

    else :
        print("Error while accessing the tilkee companies informations.")
        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return None


def get_tilkee_auth_token(tilkee_info):

    url = 'https://api.tilkee.com/oauth/token'

    headers = {"Accept": "application/json"}

    body = {
        'password': tilkee_info['password'],
        'username': tilkee_info['username'],
        'grant_type' : 'password'
    }

    r = requests.post(url=url, headers=headers, data=body)

    if r.status_code == 200:

        data = json.loads(r.text)
        print("Access Token succefully get : {}".format(data))
        return data['access_token']

    else :
        print("Error while accessing the tilkee access token.")
        print('request fail with status_code : '+str(r.status_code))
        print(r.text)
        return None
