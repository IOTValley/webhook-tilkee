# Tilkee Webhook-Pipedrive

## Context du projet

Ce projet vise à être utilisé de pair avec l'extension Chrome qui permet de générer des liens Tilkee et de les intégré automatiquement à des deals Pipedrive. Il permet d'obtenir des informations de lecture des liens générés.

## Fonctionnement de l'application

Cette app écoute l'event Tilkee "ConnexionEnd", event levé lorsque l'on quitte une présentation ouverte depuis un lien Tilkee. Lorsque cet event est levé, il est envoyé à cette app qui va ensuite chercher tous les deals Pipedrive qui contiennent dans l'un de leurs champs l'url tilkee de la présentation qui a levé l'event. Pour chacun des deals qui contiennent cet url, alors l'app créer une activité "Tilkee-View" au deals concernés. Cette activité contient :
1. le pourcentage lu de la présentation
2. le temps passé sur la présentation
3. le nombre de connexion sur la présentation

Ces informations sont mises à jour dans l'activité dans le cas ou l'on consulte à nouveau la présentation.

## Configuration de l'application

### Importer le projet Git

Il faut faut d'abord importe le projet Git :
`git clone https://gitlab.com/IOTValley/tilkee-webhook.git`

### Créer un dyno heroku

Pour cette section, il vous faut avoir un compte heroku et installer heroku [CLI](https://devcenter.heroku.com/articles/getting-started-with-python#set-up).

Une fois que heroku CLI est installé, vous devez vous connecter à votre compte heroku depuis le CLI :
`heroku login`

Une fois connecter, vous devez créer une app heroku :
`heroku create`

### Configurer et push votre projet

Il faut maintenant vous connecter a votre dashboard heroku puis ensuite aller sur la page de votre nouvelle application. Allez ensuite dans l'onglet **Settings** puis cliquez sur **reveal config var**. Vous allez alors avoir la possibilé de rentrer des variables de configurations. Vous allez devoir entrer les suivantes : 
* PIPEDRIVE_TOKEN : votre token d'API pipedrive
* TILKEE_PASSWORD : votre password Tilkee
* TILKEE_USEREMAIL : votre useremail Tilkee
* TILKEE_USERNAME : votre username Tilkee (souvent la même chose que votre useremail)
* WEB_CONCURRENCY : mettre ici la valeur **1**
* WEBHOOK_URL : mettre ici l'url de votre application avec l'endpoint "tilkee_webhook" : https://nom-de-votre-app.herokuapp.com/tilkee_webhook/

Une fois cette configuration faite, il vous faut pusher votre projet. Dans votre terminal préféré, aller dans le repértoire du projet puis lancer la commande suivante : 
`git push heroku master`
Le projet va être pusher sur la machine virtuel heroku. Votre webhook tilkee va être créer puis l'app va ce mettre à écouter sur l'url spécifié tous les event levé par Tilkee pour les traité et créer des activités Pipedrive en conséquence.

**Attention**, pour fonctionner, le projet doit detecter dans vos deal le champ personnalisable qui correspond aux liens Tilkee enregistrés dans les deals. Pour que le projet fonctionne, il **faut** que ce champ ce nomme __tilkee__, __lien_tilkee__ ou __tilkee_link__. Si vous n'avait pas ce champs dans vos deals, alors créez le, sinon, renommer le champs correspondant pour qu'il match ce critère.